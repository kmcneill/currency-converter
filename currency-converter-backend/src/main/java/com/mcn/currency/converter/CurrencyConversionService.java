package com.mcn.currency.converter;

import com.mcn.currency.model.ConversionResponse;

import java.util.Optional;

import javax.money.CurrencyUnit;
import javax.money.MonetaryAmount;
import javax.money.convert.CurrencyConversion;
import javax.money.convert.MonetaryConversions;

import org.javamoney.moneta.Money;
import org.springframework.stereotype.Service;

@Service
public class CurrencyConversionService {

    public CurrencyConversionService() {
	// Preload data on startup.
	MonetaryConversions.getExchangeRateProvider("ECB");
    }

    public Optional<ConversionResponse> convert(CurrencyUnit fromCurrency, Float amount, CurrencyUnit toCurrency) {
	Money input = Money.of(amount, fromCurrency);
	CurrencyConversion toCurrencyConversion = MonetaryConversions.getConversion(toCurrency);
	MonetaryAmount result = input.with(toCurrencyConversion);

	return Optional.ofNullable(new ConversionResponse(fromCurrency.getCurrencyCode(), amount,
		result.getCurrency().getCurrencyCode(), result.getNumber().floatValue()));
    }

}
