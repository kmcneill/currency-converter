package com.mcn.currency.rest;

import com.mcn.currency.converter.CurrencyConversionService;
import com.mcn.currency.model.ConversionRequest;
import com.mcn.currency.model.ConversionResponse;

import java.util.Optional;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.money.MonetaryException;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Validated
public class CurrencyConversionController {
    
    private static Logger LOGGER = LoggerFactory.getLogger(CurrencyConversionController.class);

    @Autowired
    private CurrencyConversionService conversionService;

    @PostMapping("/currency/convert")
    public ResponseEntity<?> convert(@RequestBody @Valid ConversionRequest request) {
    	// While RuntimeExceptions are not normally caught, this is just a way to return
	// an informative message to the user.
	try {
	    CurrencyUnit fromCurrencyUnit = Monetary.getCurrency(request.getSourceCurrency());
	    CurrencyUnit toCurrencyUnit = Monetary.getCurrency(request.getTargetCurrency());

	    Optional<ConversionResponse> response = conversionService.convert(fromCurrencyUnit, request.getSourceAmount(),
		    toCurrencyUnit);

	    return ResponseEntity.ok(response.get());
	} catch (MonetaryException ex) {
	    LOGGER.error(ex.getMessage());
	    return ResponseEntity.badRequest().body(ex.getMessage());
	}
    }

}
