package com.mcn.currency.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConversionRequest {
    
    @NotNull(message = "sourceAmount must not be null")
    private Float sourceAmount;

    @NotNull(message = "sourceCurrency must not be null")
    @Size(min = 3, max = 3, message = "sourceCurrency must consist of three letters")
    private String sourceCurrency;

    
    @NotNull(message = "targetCurrency must not be null")
    @Size(min = 3, max = 3, message = "targetCurrency must consist of three letters")
    private String targetCurrency;

}
