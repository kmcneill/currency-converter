package com.mcn.currency.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConversionResponse {

    private String sourceCurrency;

    private Float sourceAmount;

    private String targetCurrency;

    private Float targetAmount;

}
