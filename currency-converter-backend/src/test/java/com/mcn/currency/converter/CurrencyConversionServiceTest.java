package com.mcn.currency.converter;

import com.mcn.currency.model.ConversionResponse;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.money.Monetary;
import javax.money.spi.ServiceProvider;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/*
 * TODO This really needs some work, to work out how to return a result from a mock provider.
 */
class CurrencyConversionServiceTest {



    private CurrencyConversionService service = new CurrencyConversionService();

    private static ServiceProvider originalServiceProvider;

    /*
     * private static ServiceProvider mockServiceProvider =
     * Mockito.mock(ServiceProvider.class);
     * 
     * @BeforeAll static void setCurrencyProvider() { originalServiceProvider =
     * Bootstrap.getService(ServiceProvider.class);
     * Bootstrap.init(mockServiceProvider); }
     * 
     * @AfterAll static void resetCurrencyProvider() {
     * Bootstrap.init(originalServiceProvider); }
     */

    @Test
    void testConvertPoundsToEuros() {
	Optional<ConversionResponse> result = service.convert(Monetary.getCurrency("GBP"), 100_000.00F,
		Monetary.getCurrency("EUR"));

	// assertThat(result.get().getToAmount(), equalTo(101.0f));
    }

}
