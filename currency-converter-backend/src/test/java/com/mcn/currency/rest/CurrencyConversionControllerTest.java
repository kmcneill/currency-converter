package com.mcn.currency.rest;

import static org.hamcrest.CoreMatchers.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mcn.currency.converter.CurrencyConversionService;
import com.mcn.currency.model.ConversionRequest;
import com.mcn.currency.model.ConversionResponse;

import java.util.Optional;

import javax.money.CurrencyUnit;
import javax.money.Monetary;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class CurrencyConversionControllerTest {

    private static final String ENDPOINT = "/currency/convert";

    @Autowired
    private MockMvc mvc;

    @MockBean
    private CurrencyConversionService conversionService;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Test
    void testConvertEurosToPounds_ValidInput() throws Exception {
        CurrencyUnit GBP = Monetary.getCurrency("GBP");
        CurrencyUnit EUR = Monetary.getCurrency("EUR");

        ConversionRequest conversionRequest = new ConversionRequest(1.12f, GBP.getCurrencyCode(),
                EUR.getCurrencyCode());

        ConversionResponse conversionResponse = new ConversionResponse(conversionRequest.getSourceCurrency(),
                conversionRequest.getSourceAmount(),
                conversionRequest.getTargetCurrency(),
                1.27f);

        Mockito.when(conversionService.convert(GBP, conversionRequest.getSourceAmount(), EUR))
                .thenReturn(Optional.of(conversionResponse));

        String json = objectMapper.writeValueAsString(conversionRequest);

        MockHttpServletResponse response = mvc.perform(post(ENDPOINT).contentType(MediaType.APPLICATION_JSON_VALUE) //
                .content(json))//
                .andDo(MockMvcResultHandlers.print()).andExpect(status().isOk()) //
                .andReturn().getResponse();

        ConversionResponse result = objectMapper.readValue(response.getContentAsString(),
                new TypeReference<ConversionResponse>() {
                });

        assertThat(result, equalTo(conversionResponse));
    }

    @Test
    void testConvertEurosToPounds_InvalidCurrency() throws Exception {
        ConversionRequest conversionRequest = new ConversionRequest(1.12f, "FDP", "EUR");

        String json = objectMapper.writeValueAsString(conversionRequest);

        MockHttpServletResponse response = mvc.perform(post(ENDPOINT).contentType(MediaType.APPLICATION_JSON_VALUE) //
                .content(json))//
                .andDo(MockMvcResultHandlers.print()).andExpect(status().is(400)) //
                .andReturn().getResponse();

        assertThat(response.getContentAsString(), equalTo("Unknown currency code: FDP"));
    }

    @Test
    void testConvertEurosToPounds_CurrencyCodeTooSmall() throws Exception {
        testValidationError(new ConversionRequest(1.12f, "FD", "EUR"), //
                "sourceCurrency must consist of three letters");
    }

    @Test
    void testConvertEurosToPounds_CurrencyCodeTooLarge() throws Exception {
        testValidationError(new ConversionRequest(1.12f, "FDBA", "EUR"), //
                "sourceCurrency must consist of three letters");
    }

    @Test
    void testConvertEurosToPounds_NullCurrencyCode() throws Exception {
        testValidationError(new ConversionRequest(1.12f, null, "EUR"), //
                "sourceCurrency must not be null");
    }

    @Test
    void testConvertEurosToPounds_NullCurrencyAmount() throws Exception {
        testValidationError(new ConversionRequest(null, "GBP", "EUR"), //
                "sourceAmount must not be null");
    }

    private void testValidationError(ConversionRequest conversionRequest, String expectedErorMessage)
            throws JsonProcessingException, Exception {
        String json = objectMapper.writeValueAsString(conversionRequest);

        MvcResult response = mvc.perform(post(ENDPOINT).contentType(MediaType.APPLICATION_JSON_VALUE) //
                .content(json))//
                .andDo(MockMvcResultHandlers.print()).andExpect(status().is(400)) //
                .andReturn();

        Exception resolvedException = response.getResolvedException();
        assertThat(resolvedException.getMessage(), containsString(expectedErorMessage));
    }

}
