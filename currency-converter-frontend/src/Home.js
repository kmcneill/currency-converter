import React, { Component } from 'react';
import './App.css';
import AppNavbar from './AppNavbar';
import { Container, FormGroup, Input, Label } from 'reactstrap';
import { ConversionRequest, CURRENCY_CODES_COMMON } from './model/CurrencyConversionModels';


let options = [];
CURRENCY_CODES_COMMON.map(item =>
    options.push({ label: item, value: item.split(' - ')[0] }),
);

class Home extends Component {

    baseItem = {
        sourceAmount: '',
        sourceCurrency: 'EUR',
        targetCurrency: 'GBP',
        alertClass: 'alert alert-secondary',
        alertDisplay: 'none',
        response: ''
    };

    constructor(props) {
        super(props);
        this.state = {
            item: this.baseItem
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleReset = this.handleReset.bind(this);
        this.hideAlert = this.hideAlert.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const { item } = this.state;

        let req = new ConversionRequest(item.sourceAmount, item.sourceCurrency, item.targetCurrency);
        let asJson = JSON.stringify(req);
        console.info('REQUEST = ' + req);

        await fetch('/currency/convert', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: asJson,
        })
            .then(async response => {
                if (!response.ok) {
                    const text = await response.text();
                    console.error(response);
                    const errorMsg = response.status + ' - ' + response.statusText + '  --  ' + text;
                    return Promise.reject(errorMsg);
                } else {
                    console.info('Response OK')
                    const data = await response.json();
                    item.response = data.targetAmount;
                    item.alertClass = 'alert alert-success';
                }
            })
            .catch(error => {
                console.error('There was an error!', error);
                item.response = error;
                item.alertClass = 'alert alert-danger';
            });
        item.alertDisplay = "block"
        this.setState({ item });
    }

    handleReset(event) {
        const item = this.baseItem;
        this.setState({ item });
    }

    handleChange(event) {
        const target = event.target;
        const name = target.name;
        const value = target.value;
        
        let item = { ...this.state.item };
        item[name] = value;

        this.setState({ item });
    }

    hideAlert(event) {
        let item = { ...this.state.item };
        item.alertDisplay = 'none';
        this.setState({ item });
    }

    render() {
        const { item } = this.state;
        return (
            <div>
                <AppNavbar />
                <Container >

                    <fieldset style={{ padding: '1em', border: '0.1em solid black' }}>

                        <form id='myForm' onSubmit={this.handleSubmit}>
                            <FormGroup>
                                <Label for="sourceAmount">SourceAmount</Label>
                                <Input name="sourceAmount" id="sourceAmount" value={item.sourceAmount || ''}
                                    onChange={this.handleChange}
                                    placeholder='Enter Source Amount, e.g. 10.25' title='Enter Source Amount, e.g. 10.25'
                                    required="required" />
                            </FormGroup>
                            <FormGroup>
                                <Label for="sourceCurrency">Source Currency</Label>
                                <Input name="sourceCurrency" id="sourceCurrency" type='select'
                                    value={item.sourceCurrency} onChange={this.handleChange} required="required" >
                                    {
                                        options.map((item) =>
                                            <option value={item.value}>{item.label}</option>
                                        )
                                    }
                                </Input>
                            </FormGroup>
                            <FormGroup>
                                <Label for="targetCurrency">Target Currency</Label>
                                <Input name="targetCurrency" id="targetCurrency" type='select'
                                    value={item.targetCurrency} onChange={this.handleChange} required="required" >
                                    {
                                        options.map((item) =>
                                            <option value={item.value}>{item.label}</option>
                                        )
                                    }
                                </Input>
                            </FormGroup>
                            <FormGroup>
                                <div className="clearfix" style={{ padding: '.5rem' }}>
                                    <button className="btn btn-primary float-left" type='submit'>Submit</button>
                                    <input className="btn btn-secondary float-right" type='reset'
                                        onClick={this.handleReset}></input>
                                </div>
                            </FormGroup>
                            <FormGroup>
                                <div id='resultAlert' className={this.state.item.alertClass} style={{ display: item.alertDisplay }}>
                                    {this.state.item.response || ''}
                                    <button type="button" className="close" aria-label="Close"
                                        onClick={this.hideAlert}>
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </FormGroup>
                        </form>
                    </fieldset>
                </Container>
            </div>
        );
    }

}

export default Home;
