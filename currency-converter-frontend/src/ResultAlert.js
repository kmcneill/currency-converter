import React, { useState } from 'react';
import { Alert } from 'reactstrap';

const ResultAlert = (props) => {
  const [visible, setVisible, message, color] = useState(true);

  const onDismiss = () => setVisible(false);

  return (
    <Alert color={color} isOpen={visible} toggle={onDismiss}>
      <span>{message}</span>
    </Alert>
  );
}

export default ResultAlert;
