export class ConversionRequest {
    constructor(sourceAmount, sourceCurrency, targetCurrency) {
        this.sourceAmount = sourceAmount;
        this.sourceCurrency = sourceCurrency;
        this.targetCurrency = targetCurrency;
    }
}

export class ConversionResponse extends ConversionRequest {
    constructor(sourceAmount, sourceCurrency, targetCurrency, targetAmount) {
        super(sourceAmount, sourceCurrency, targetCurrency);
        this.sourceAmount = sourceAmount;
        this.sourceCurrency = sourceCurrency;
        this.targetAmount = targetAmount;
    }
}

export let CURRENCY_CODES_COMMON = [
"USD - US dollar",
"EUR - Euro",
"JPY - Japanese yen",
"BGN - Bulgarian lev",
"CZK - Czech koruna",
"DKK - Danish krone",
"GBP - Pound sterling",
"HUF - Hungarian forint",
"PLN - Polish zloty",
"RON - Romanian leu",
"SEK - Swedish krona",
"CHF - Swiss franc",
"ISK - Icelandic krona",
"NOK - Norwegian krone",
"HRK - Croatian kuna",
"RUB - Russian rouble",
"TRY - Turkish lira",
"AUD - Australian dollar",
"BRL - Brazilian real",
"CAD - Canadian dollar",
"CNY - Chinese yuan renminbi",
"HKD - Hong Kong dollar",
"IDR - Indonesian rupiah",
"ILS - Israeli shekel",
"INR - Indian rupee",
"KRW - South Korean won",
"MXN - Mexican peso",
"MYR - Malaysian ringgit",
"NZD - New Zealand dollar",
"PHP - Philippine peso",
"SGD - Singapore dollar",
"THB - Thai baht",
"ZAR - South African rand",

];

export let CURRENCY_CODES = ["SVC", "ISK", "JMD", "COU", "SAR", "TTD", "SBD", "SHP", "CHW", "GBP", "TRY", "MOP",
    "INR", "XDR", "MAD", "CUP", "CLF", "SZL", "SRD", "IRR", "EUR", "KZT", "CHF", "ALL", "MZN", "CDF", "BZD", "KES",
    "AOA", "NGN", "SEK", "XAU", "BND", "UYU", "BRL", "YER", "ANG", "CHE", "DJF", "LSL", "BBD", "MGA", "VND", "DOP",
    "TJS", "FJD", "BOV", "BGN", "TWD", "XTS", "QAR", "VES", "CNY", "AUD", "ZMW", "WST", "MDL", "KHR", "NZD", "AMD",
    "AZN", "TZS", "SSP", "GIP", "AED", "XPT", "AFN", "DZD", "PKR", "KYD", "AWG", "BSD", "XAF", "PEN", "NAD", "XPD",
    "BTN", "KRW", "UYW", "NPR", "EUR", "FKP", "TND", "ZWL", "MWK", "JOD", "LRD", "RSD", "XBA", "SCR", "HUF", "IDR",
    "LAK", "UAH", "XAG", "ZAR", "OMR", "PGK", "SLL", "MVR", "USD", "MXV", "MKD", "COP", "CAD", "KMF", "MXN", "PYG",
    "XCD", "XBB", "IQD", "TOP", "VUV", "SOS", "CVE", "CUC", "KGS", "TMT", "THB", "BOB", "UZS", "BYN", "ILS", "PLN",
    "RON", "USD", "BHD", "UYI", "BIF", "XPF", "ARS", "GTQ", "RUB", "XSU", "HKD", "XBC", "RWF", "BDT", "ETB", "LYD",
    "JPY", "STN", "SGD", "MNT", "NIO", "USN", "GEL", "NOK", "MUR", "ERN", "SYP", "MYR", "DKK", "HRK", "XBD", "CLP",
    "XXX", "GNF", "GMD", "BAM", "HTG", "LBP", "XUA", "PHP", "EGP", "SDG", "CRC", "CZK", "KWD", "KPW", "GHS",
    "GYD", "LKR", "BMD", "BWP", "MRU", "PAB", "UGX", "MMK", "XOF", "HNL"];


